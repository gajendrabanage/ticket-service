package com.test.ticket.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.test.ticket.entity.Ticket;

public interface TicketRepository extends JpaRepository<Ticket, Long>{

}
