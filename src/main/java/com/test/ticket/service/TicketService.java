package com.test.ticket.service;

import com.test.ticket.dto.TicketDTO;

public interface TicketService {

	void ticketBook(TicketDTO ticketDTO);

}
