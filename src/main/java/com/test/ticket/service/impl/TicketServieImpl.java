package com.test.ticket.service.impl;

import javax.jms.Queue;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.jms.InsuranceRquestDTO;
import com.test.ticket.dto.TicketDTO;
import com.test.ticket.entity.Ticket;
import com.test.ticket.repository.TicketRepository;
import com.test.ticket.service.TicketService;

@Service
public class TicketServieImpl implements TicketService{

	@Autowired
	private TicketRepository ticketRepository;
	
	@Autowired
	private JmsMessagingTemplate jmsMessagingTemplate;
 
	
	@Override
	public void ticketBook(TicketDTO ticketDTO) {

		Ticket ticket = new Ticket();
		BeanUtils.copyProperties(ticketDTO, ticket);
		ticket.setStatus("PENDING");
		Ticket persistedTicket = ticketRepository.save(ticket);
		
		System.out.println(persistedTicket);
		this.jmsMessagingTemplate.convertAndSend(new ActiveMQQueue("insurance"), new InsuranceRquestDTO(persistedTicket.getId(),
				persistedTicket.getPrice(), persistedTicket.getUserId()));
			
	}

}
