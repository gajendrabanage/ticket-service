package com.test.ticket.service.impl;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.test.ticket.dto.TicketDTO;

@Component
public class MessageReceiver {
 
	//@JmsListener(destination = "insurance1", containerFactory = "myFactory1")
	public void receiveQueue(TicketDTO ticketDTO) {
		
		System.out.println("Message Received: "+ticketDTO);
	}
}
 
