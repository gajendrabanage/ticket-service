package com.test.ticket.listner;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.test.jms.PaymentResponseDTO;
import com.test.ticket.entity.Ticket;
import com.test.ticket.repository.TicketRepository;

@Component
public class TicketListiner  {

	
	@Autowired
	private TicketRepository ticketRepository;
	
	@JmsListener(destination = "paymentResponse", containerFactory = "topicConnectionFactory") 
	public void receiveQueue(PaymentResponseDTO paymentResponseDTO) {
		
		System.out.println("update :"+paymentResponseDTO);

		if (paymentResponseDTO.getStatus().equals("DONE")) {
		Optional<Ticket> tickets =	ticketRepository.findById(paymentResponseDTO.getTicketId());
		
		if (tickets.isPresent()) {
			Ticket ticket = tickets.get();
			ticket.setStatus("BOOKED");
			ticketRepository.save(ticket);
		}
		}
		
	}
	
}
