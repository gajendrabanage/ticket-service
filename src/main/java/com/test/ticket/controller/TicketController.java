package com.test.ticket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.test.ticket.dto.TicketDTO;
import com.test.ticket.service.TicketService;

@RestController
public class TicketController {

	
	@Autowired
	private TicketService ticketService;
	
	@PostMapping("ticket") 
	public void ticketBook(@RequestBody TicketDTO ticketDTO) {
		
		ticketService.ticketBook(ticketDTO);
	} 
}
